<?php
namespace Edsplugins\GoogleMaps;
use \Edsplugins\Helper\Helper as Helper;

Class GoogleMaps {

    protected $page;

    public function __construct()
    {
        $this->page = new Helper;
        $this->page->addPage(
            'GooglesMaps',
            'EdsGoogleMaps',
            'update_core',
            'eds-google-maps',
            'GoogleMapsHome.php',
            '',
            ''
        );
    }
}