<?php
namespace Edsplugins\Helper;

Class Helper {

    /**
     * Stores the page name for the Add Page/Sub-Page Helper function
     *
     * @var string
     */
    public $page_name;

    /**
     * Stores the menu title for the Add Page/Sub-Page Helper function
     *
     * @var string
     */
    public $menu_title;

    /**
     * Stores what capability type should be used when creating a page
     *
     * @var string
     */
    public $capability;

    /**
     * The slug for any page created using the Add Page/Sub-Page Helper function
     *
     * @var string
     */
    public $slug;

    /**
     * The callback function for page display
     *
     * @var function
     * @return string Returns a URL for the content of each page
     */
    public $content;

    /**
     * Stores the url to an icon
     *
     * @var string
     */
    public $icon_url;

    /**
     * What position to display the menu item
     *
     * @var integer
     */
    public $position;

    public function __construct()
    {
    }

    /**
     * A Helper function to create new admin pages
     *
     * @param string $page_name
     * @param string $menu_title
     * @param string $capability
     * @param string $slug
     * @param function $page_content
     * @param string $icon_url
     * @param integer $position
     * @return void
     */
    public function addPage($page_name = null, $menu_title = null, $capability = null, $slug = null, $page_content = null, $icon_url = null, $position = null)
    {   
        // Setting the public variables defined earlier with values passed to function
        $this->page_name = $page_name;
        $this->menu_title = $menu_title;
        $this->capability = $capability;
        $this->slug = $slug;
        $this->content = $page_content;
        $this->icon_url = $icon_url;
        $this->$position = $position;

        add_action( 'admin_menu', [$this, 'addPageFunction'], 12);
    }

    /**
     * Returns a file to include
     *
     * @param string $content
     * @return string Returns a url provided $content isn't null
     */
    public function addPageContent() {
        return include plugin_dir_path( dirname(__FILE__) ) . 'Views/' . $this->content;
    }

    /**
     * The function to pass into the Add Page Helper Function
     *
     * @return void
     */
    public function addPageFunction() {
        add_menu_page(
            $this->page_name,
            $this->menu_title,
            $this->capability,
            $this->slug,
            array($this, 'addPageContent'),
            $this->icon_url,
            $this->position
        );
    }
}