<?php
/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the
 * plugin admin area. This file also includes all of the dependencies used by
 * the plugin, registers the activation and deactivation functions, and defines
 * a function that starts the plugin.
 *
 * @link              
 * @since             0.1.0
 * @package           eds-plugins
 *
 * @wordpress-plugin
 * Plugin Name:       Ed's Plugins
 * Plugin URI:        
 * Description:       Learn how to use Namespaces and Autoloading in WordPress.
 * Version:           0.1.0
 * Author:            Ed Bennett
 * Author URI:        
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 */
if( !defined( 'WPINC' ) ) die;
require "vendor/autoload.php";

use Edsplugins\GoogleMaps\GoogleMaps as GoogleMapsRun;

$maps = new GoogleMapsRun();